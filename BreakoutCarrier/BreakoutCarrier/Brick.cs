﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutCarrier
{
    class Brick
    {
        Texture2D texture;
        Rectangle location;//used to determine boundaries of each night hawk image
        Color tint;// color of the nighthawk image
        bool alive;//used for collision detection with the jet

        public Rectangle Location
        {
            get { return location; }
        }

        /*
         * Constructor for the nighthawk.  This will be used to create a new nighthawk
         * object for each space in the matrix.
         * 
         * @param: texture, location, tint
         */ 
        public Brick(Texture2D texture, Rectangle location, Color tint)
        {
            this.texture = texture;
            this.location = location;
            this.tint = tint;
            this.alive = true;
        }

        //determines if the jet has collided with the nighthawk and will erase the object
        //from the matrix if collision has occured.
        public Boolean CheckCollision(Ball ball)
        {
            if (alive && ball.Bounds.Intersects(location))
            {
                alive = false;
                ball.Deflection(this);
                return true;
            }
            return false;
        }
        

        //standard draw for the nighthawk object. 
        public void Draw(SpriteBatch spriteBatch)
        {
            if (alive)
                spriteBatch.Draw(texture, location, tint);
        }
    }
}
