﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutCarrier
{
    class Ball
    {
        Vector2 motion;
        Vector2 position;
        Rectangle bounds;
        bool collided;//true if collions occurs false if no collision

        const float ballStartSpeed = 10f;//initial speed of the jet when startgame is executed
        float ballSpeed;//updated as the jet experiences collision and changes speeds

        Texture2D texture;
        Rectangle screenBounds;

        public Rectangle Bounds
        {
            get
            {
                bounds.X = (int)position.X;
                bounds.Y = (int)position.Y;
                return bounds;
            }
        }

        public Ball(Texture2D texture, Rectangle screenBounds)
        {
            bounds = new Rectangle(0, 0, texture.Width, texture.Height);
            this.texture = texture;
            this.screenBounds = screenBounds;
        }

        public void Update()
        {
            collided = false;
            position += motion * ballSpeed;
            ballSpeed += 0.001f;

            CheckWallCollision();
        }


        private void CheckWallCollision()
        {
            if (position.X < 0)//has hit left wall
            {
                position.X = 0;
                motion.X *= -1;
            }
            if (position.X + texture.Width > screenBounds.Width)//has hit right wall
            {
                position.X = screenBounds.Width - texture.Width;
                motion.X *= -1;
            }
            if (position.Y < 0)//has hit the top of the screen.
            {
                position.Y = 0;
                motion.Y *= -1;
            }
        }

        //places the jet in its firts postion when the gamestart method is executed 
        //will set the jet speed as well as determine the placement and
        //where the jet is moving
        public void SetInStartPosition(Rectangle paddleLocation)
        {
            Random rand = new Random();

            motion = new Vector2(rand.Next(2, 6), -rand.Next(2, 6));
            motion.Normalize();

            ballSpeed = ballStartSpeed;

            position.Y = paddleLocation.Y - texture.Height;
            position.X = paddleLocation.X + (paddleLocation.Width - texture.Width) / 2;
        }

        public bool OffBottom()
        {
            if (position.Y > screenBounds.Height)
                return true;
            return false;
        }

        //Calculatis the current position of the jet and carrier. 
        //checks for collision between the two objects and then calulated the
        //deflection of the jet after the collision occurs.
        public void PaddleCollision(Rectangle paddleLocation)
        {
            Rectangle ballLocation = new Rectangle(
                (int)position.X,
                (int)position.Y,
                texture.Width,
                texture.Height);

            if (paddleLocation.Intersects(ballLocation))
            {
                position.Y = paddleLocation.Y - texture.Height;
                motion.Y *= -1;
            }
        }

        //This calculated the deflection path and speed after a collision with the
        //nighhawk occurs
        public void Deflection(Brick brick)
        {
            if (!collided)
            {
                motion.Y *= -1;
                collided = true;
            }
        }

        //Standard draw routine for the jet sprite which is used as the ball
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }
    }
}