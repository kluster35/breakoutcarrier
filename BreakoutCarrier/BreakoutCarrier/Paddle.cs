﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

/*
 * This class is named paddle and will be used to instantiate an object to deflect the 
 * ball from colliding with the bottom of the screen. in this case it will draw a 
 * picture of an aircraft carrier on the screen and have the functionality of moving 
 * side to side and collision detection. 
 * 
 * @Constructors: Single 2 parameter constructor for a paddle instance. 
 * @Methods: GetBounds()
 *           Update()
 *           LockPaddle()
 *           SetInStartPosition()
 *           Draw(SpriteBatch spriteBatch)
 */
namespace BreakoutCarrier
{
    class Paddle
    {
        Vector2 position; //horizontal vector for paddle object to move along
        Vector2 motion; //the delta between the current position of paddle and where it needs to be
        float paddleSpeed = 8f; //moves by 8pixels at a time

        KeyboardState keyBoardState;
        GamePadState gamePadState;

        Texture2D texture;
        Rectangle screenBounds; //used to determine if paddle has reached the left/right boundary


        /*
         * Creates instnace of a paddle object.  The texture variable is used for drawing 
         * screenBounds is passed in from the Game1 class.  Calls set in start position 
         * to begin the game. 
         * @ param: "Texture2D texture" variable which resolves to the picture of the 
         *          aircraft carrier.
         *          "Rectangle screenBounds" the boundaries of the gaming area from game1 class
         *  
         */
        public Paddle(Texture2D texture, Rectangle screenBounds)
        {
            this.texture = texture;
            this.screenBounds = screenBounds;
            SetInStartPosition();
        }

        /*
         * The primary use of the paddle update is to check if the paddle (aircraft carrier)
         * has moved.  It checks the state of the keyboard and moves the paddle left or right
         * based on the motion and position.  The position is changed by a factor of the 
         * motion vector which moves left or right by the speed whenever an arrow key is 
         * being pressed. 
         * @ param: no parameters
         * @ return: no return value
         */ 
        public void Update()
        {
            motion = Vector2.Zero;

            keyBoardState = Keyboard.GetState();
            gamePadState = GamePad.GetState(PlayerIndex.One);

            if (keyBoardState.IsKeyDown(Keys.Left) || gamePadState.IsButtonDown(Buttons.LeftThumbstickLeft) ||
                gamePadState.IsButtonDown(Buttons.DPadLeft))
                motion.X = -1;

            if (keyBoardState.IsKeyDown(Keys.Right) ||
                gamePadState.IsButtonDown(Buttons.LeftThumbstickLeft) ||
                gamePadState.IsButtonDown(Buttons.DPadLeft))
                motion.X = 1;

            motion.X *= paddleSpeed;
            position += motion;
            LockPaddle();
        }

        /*
         * Keeps the paddle in place if it has reached the left/right boundary of the 
         * game screen. 
         * @ param: no parameters
         * @ return: no return value
         */ 
        public void LockPaddle()
        {
            if (position.X < 0)
                position.X = 0;
            if (position.X + texture.Width > screenBounds.Width)
                position.X = screenBounds.Width - texture.Width;
        }

        /*
         * Places the paddle (aircraft carrier in center of the game screen in the first 
         * draw operation of the game. 
         * 
         * @param: no parameters
         * @return: no return value
         */
        public void SetInStartPosition()
        {
            position.X = (screenBounds.Width - texture.Width) / 2;
            position.Y = screenBounds.Height - texture.Height - 5;
        }

        /*
         *Places the image of the carrier on the screen. Executed 60x/sec by game1 class. 
         *@param: "SpriteBatch spriteBatch" this will resolve to the sprite for this paddle
         *          class which is the carrier.png file. 
         *@return: no return value
         * 
        */
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, position, Color.White);
        }

        /*
         *returns a rectangle object. This is used in collision detection 
         *with the ball. This will return the boundaries of the carrier
         *rectangle.
         *
         * @param: no parameters
         * @return: a rectangle object to be used for collision detection with the "ball/jet"
        */  
        public Rectangle GetBounds()
        {
            return new Rectangle(
                (int)position.X,
                (int)position.Y,
                texture.Height,
                texture.Width);
        }
    }
}