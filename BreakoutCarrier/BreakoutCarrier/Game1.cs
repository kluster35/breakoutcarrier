using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace BreakoutCarrier
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;//used to draw the carrier, jet, and hawks

        Texture2D background;//places the image of the pacific ocean in background
        Rectangle mainFrame;

        Ball ball;//this is a object that will be the jet
        Paddle paddle;//aircraft carrier object
        Rectangle screenRectangle;

        int bricksWide = 17;//number of nighthawk columns
        int bricksHigh = 5;//number of rows of nighthawks

        Texture2D brickImage;
        Brick[,] bricks;//matrix of "bricks" which will be the nighthawks
        Score gameScore; //Score object that increase by ten everytime collision with nighthawk occurs. Located in upper left corner
        GameOver gameOverScreen;//splash screen once the game ends

        /*
         * Constructor for the game.  This will setup the screen boundaries
         * and set the directory where content can be found. 
         * 
         * @param: no parameters.
         */ 
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1400;
            graphics.PreferredBackBufferHeight = 865;

            screenRectangle = new Rectangle(
                0,
                0,
                graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            //loads the carrier sprite and instantates the carrier object.
            Texture2D tempTexture = Content.Load<Texture2D>("carrier");
            paddle = new Paddle(tempTexture, screenRectangle);

            //loads teh jet content sprite and instantiates the jet object.
            tempTexture = Content.Load<Texture2D>("jet");
            ball = new Ball(tempTexture, screenRectangle);

            //loads the nighthawk sprite
            brickImage = Content.Load<Texture2D>("nighthawk");
            
            //loads the pacific ocean image and instantiates rectangle object for screen
            background = Content.Load<Texture2D>("PacificOcean");
            mainFrame = new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);
            
            //instantiates new score object and loads the font.
            gameScore = new Score();
            gameScore.Font = Content.Load<SpriteFont>("Arial");

            StartGame();
        }

        /*
         * Places aircraft carrier and jet in the middle of the bottom of the screen and 
         * loads the nighhawk sprites into the brick matrix.
         * 
         * @param: no parameters
         * @return: no return value
         */ 
        public void StartGame()
        {
            gameOverScreen = null;
            paddle.SetInStartPosition();
            ball.SetInStartPosition(paddle.GetBounds());

            //instantiate matrix of nighthawk sprites "bricks"
            bricks = new Brick[bricksWide, bricksHigh];
            loadMatrix(); 
        }

        public void loadMatrix()
        {
            //loading the matrix
            for (int y = 0; y < bricksHigh; y++)
            {
                Color tint = Color.White;

                for (int x = 0; x < bricksWide; x++)
                {
                    bricks[x, y] = new Brick(
                        brickImage,
                        new Rectangle(
                            x * brickImage.Width,
                            y * brickImage.Height,
                            brickImage.Width,
                            brickImage.Height),
                        tint);
                }
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            if (gameOverScreen != null)
                gameOverScreen.Update();

            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            paddle.Update();
            ball.Update();

            //checks if jet has collided with nighthawk
            foreach (Brick brick in bricks)
            {
                if (brick.CheckCollision(ball) == true)
                    gameScore.score += 10;
            }


            ball.PaddleCollision(paddle.GetBounds());

            //checks for gameover condition
            if (ball.OffBottom())
            {
                gameScore.score = 0;
                EndGame();
                this.EndGame();
            }     

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            
            spriteBatch.Draw(background, mainFrame, Color.White);
            
            //draws the matrix of bricks
            foreach (Brick brick in bricks)
                brick.Draw(spriteBatch);

            //draws aircraft carrier
            paddle.Draw(spriteBatch);
            ball.Draw(spriteBatch);//draws jet
            gameScore.Draw(spriteBatch);//draws the score in upper left corner
            
            //will draw game over screen if the game has ended
            if (gameOverScreen != null)
                gameOverScreen.Draw(spriteBatch);
            
            spriteBatch.End();

            base.Draw(gameTime);
        }

    /*
     * Draws the gameover screen.
     * 
     * @param: no parameters
     * @return: no return value
     */ 
    public void EndGame()
    {
        gameOverScreen = new GameOver(this);
    }

    }
}